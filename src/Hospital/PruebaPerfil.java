
package Hospital;
import java.util.Scanner;
/**
 *
 * @author Ximena Puchaicela
 */
class PerfilMedico{
    public String nombre;
    public String apellido;
    public String sexo;
    public int dia;
    public String mes;
    public int anio;
    private double altura;
    private int peso;
    public PerfilMedico(String nombre, String apellido, String sexo, int dia, String mes, int anio, double altura, int peso) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.sexo = sexo;
        this.dia = dia;
        this.mes = mes;
        this.anio = anio;
        this.altura = altura;
      this.peso = peso;
    }
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public int getDia() {
        return dia;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public double getAltura() {
        return altura/100;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    public int getPeso() {
        return peso;
    }

    public void setPeso(int peso) {
        this.peso = peso;
    }
    public int edad(){
        int edad;
        final int anio_actual =2018;
        edad= anio_actual-this.anio;
        return edad;
    }
    public int frecuenciaCardiaca(){
       int frecuencia;
       frecuencia = 220-edad();
       return frecuencia;
    }
    public String rangoFrecuencia(){
        double max,min;
        max = frecuenciaCardiaca()*0.50;
        min = frecuenciaCardiaca()*0.85;
        return " Entre: "+max+" y "+min;
    }
    public double BMI(){
        double masa;
        masa = this.peso/(Math.pow(getAltura(), 2));
        return masa;
    }
    public String tabla(){
        return "TABLA DE REFERENCIAS DEL BMI\nBajo peso: menos de 18.5\nNormal: entre 18.5 y 24.9\nSobrepeso: entre 25 y 29.9\nObeso: 30 o mas";
    }
    @Override
    public String toString(){
    return "Nombre: "+nombre+"\nApellido: "+apellido+"\nSexo: "+sexo+"\nFecha de nacimiento: "+dia+"/"+mes+" "
            + "/"+anio+"\nAnios: "+edad()+"\nFrecuencia Cardiaca: "+frecuenciaCardiaca()+"\nRango de Frecuencia: "+rangoFrecuencia();
    }
}
public class PruebaPerfil{
    public static void main(String[] args) {
        double rango;
        String nombre,apellido,sexo,mes;
        int anio,dia,altura,peso;
        Scanner teclado = new Scanner(System.in);
        System.out.print("Ingrese el nombre del paciente: ");nombre=teclado.next();
        System.out.print("Ingrese el apellido del paciente: ");apellido=teclado.next();
        System.out.print("Ingrese el sexo del paciente: ");sexo=teclado.next();
        System.out.print("Ingrese el dia de nacimiento: ");dia=teclado.nextInt();
        System.out.print("Ingrese el mes de nacimiento(letras):  ");mes=teclado.next();
        System.out.print("Ingrese el anio de nacimiento: ");anio=teclado.nextInt();
        System.out.print("Ingrese su altura(cm): ");altura=teclado.nextInt();
        System.out.print("Ingrese su peso(kg): ");peso=teclado.nextInt();
        PerfilMedico paciente1= new PerfilMedico(nombre,apellido,sexo,dia,mes,anio,altura,peso);
        rango = paciente1.BMI();
        System.out.format(paciente1+"\nSu masa corporal es:  %.2f\n ",rango);
        System.out.println(paciente1.tabla());
    }
}

